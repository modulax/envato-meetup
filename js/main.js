$(document).ready(function()
{


var locations= [];
getLocations();


var m_names = new Array("January", "February", "March", 
"April", "May", "June", "July", "August", "September", 
"October", "November", "December");


var pastEventsCount = 0;

function getLocations()
{
		$.ajax({
		  type: "POST",
		  url: 'https://api.nvite.com/events/search?media_term=envatomeetup',
		  dataType: 'jsonp',
		  data: {}
		  })
			  .done(function(msg) 
			  {	

			  	
			  	$.each( msg, function( key, value ) 
			  	{
  					
            // GET ALL OF THE EVENTS
  					var name = value.name;

            // MAP INFO
  					var loc = value.venue.latlon;

  					loc = loc.replace('(','');
  					loc =loc.replace(')','');
  					var latlong = loc.split(',');
  					
  					var la = [name ,latlong[0], latlong[1], key, value.short_url];
  					

            // CREATE THE TABLE
            var eventDate = value.start;
            eventDate = eventDate.split('T');
            eventDate = eventDate[0];
            
            
            date = eventDate.split('-');
            var month = date[1];

            if(month < 10)
            {
              month = month.replace('0','');

            }
            finalDate = m_names[month-1] + ' ' + date[2] + ", " + date[0];


          

            // CHECK ID DATE IS PASSED
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd='0'+dd
            } 

            if(mm<10) {
                mm='0'+mm
            } 

            today = yyyy+'-'+mm+'-'+dd;


            var tableData = "";
            tableData += '<tbody>';
            tableData += '<tr class="' + key + '">';
            tableData += '<td class="itemIcon"><div class="iconDay">' + date[2] +'</div><div class="iconMonth">' +m_names[month-1].substring(0, 3) + '</div></td>';
            tableData += '<td class="itemInfo">';
            tableData += '<h3 class="heading"><a href="' + value.short_url + ' " target="_blank">' + value.name + '</a></h3>';
            tableData += '' + finalDate + '';
            tableData += '</td>';
            tableData += '<td class="itemCity">' + value.venue.vicinity + '</td>';
            tableData += '</tr>';



          
            
            // CHECK DATE
            if(eventDate > today) 
            {
                
                  $("#currentEvents").append(tableData);
                  locations.push(la);
                  
            }else
            {
                if(pastEventsCount<5)
                {
                  $("#pastEvents").append(tableData);
                  ++pastEventsCount;
                }
            }
            

           
            
  					
  					
				  });
				// console.log(locations);

				addPin();  	

			  })
			  .fail(function() {
			    console.log( "An error occured sorry! Try again." );
			  });

}

//************* MAPS *************/
function addPin()
{

var styles = [
    {
     "featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]
    }
  ];



   var styledMap = new google.maps.StyledMapType(styles,
    {name: "Styled Map"});


	var map = new google.maps.Map(document.getElementById('map'), 
	{
      zoom: 2,
      center: new google.maps.LatLng(15,14),
      mapTypeControlOptions: 
      {
     	  mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
       }

    });

  //Associate the styled map with the MapTypeId and set it to display.
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');

  var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) 
    {  

      marker = new google.maps.Marker(
      {
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        url: locations[i][4],
        indexer: locations[i][3],
        map: map
      });

      google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
        return function() 
        {
          infowindow.setContent("<h1>" + locations[i][0] + "</h1>");
          infowindow.open(map, marker);

          
        }
      })(marker, i));

      // assuming you also want to hide the infowindow when user mouses-out
      google.maps.event.addListener(marker, 'mouseout', function() {
          infowindow.close();
      });


        google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
         
          // window.location.href = this.url;
          scrollToAnchor(this.indexer);
          // console.log("Hello!");
          
        }
      })(marker, i));





    }


}


function scrollToAnchor(aid){
    // var aTag = $("a[name='"+ aid +"']");
    console.log('Class = '+ aid);
    
    var aTag = $('.' + aid);
    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
}





});