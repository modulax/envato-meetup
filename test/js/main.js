$(document).ready(function()
{

var key = "AIzaSyCbYq3UeAlVmS4WfoaE50MYg0GIA78_-hg";
var locations= [];
// addPin();
getLocations();

function getLocations()
{
		$.ajax({
		  type: "POST",
		  url: 'https://api.nvite.com/events/search?media_term=envatomeetup',
		  dataType: 'jsonp',
		  data: {}
		  })
			  .done(function(msg) 
			  {	

			  	
			  	$.each( msg, function( key, value ) 
			  	{
  					
  					var name = value.name;
  					var loc = value.venue.latlon;
  					loc = loc.replace('(','');
  					loc =loc.replace(')','');
  					var latlong = loc.split(',');
  					

  					var la = [name ,latlong[0], latlong[1]];
  					locations.push(la);
  					
  					
				});
				console.log(locations);
				addPin();
				// $("#data").html(locations);
				

			 
			  	

			  })
			  .fail(function() {
			    console.log( "An error occured sorry! Try again." );
			  });

}


function addPin()
{

var styles = [
    {
      "stylers": [
      { "hue": "#1fb4da" },
      { "invert_lightness": true },
      { "weight": 0.6 },
      { "lightness": 34 },
      { "gamma": 0 },
      { "saturation": 2 }
    ]
    }
  ];



   var styledMap = new google.maps.StyledMapType(styles,
    {name: "Styled Map"});


	var map = new google.maps.Map(document.getElementById('map'), 
	{
      zoom: 2,
      center: new google.maps.LatLng(28,16),
      mapTypeControlOptions: 
      {
     	 mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
    }

    });

     //Associate the styled map with the MapTypeId and set it to display.
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }


}






});